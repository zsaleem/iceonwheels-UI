import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { HttpClientModule } from '@angular/common/http';
import { BackgroundFetch } from '@ionic-native/background-fetch';
import { BackgroundMode } from '@ionic-native/background-mode';
// import { Push } from '@ionic-native/push';

import { MyApp } from './app.component';
import { HomePageModule } from '../pages/home/home.module';
import { NotificationsPageModule } from '../pages/notifications/notifications.module';
import { TrackPageModule } from '../pages/track/track.module';
import { ContactPageModule } from '../pages/contact/contact.module';
import { WebsitePageModule } from '../pages/website/website.module';
import { SoftwarePageModule } from '../pages/software/software.module';
import { BackendPageModule } from '../pages/backend/backend.module';
import { ListPageModule } from '../pages/list/list.module';
import { MenuPageModule } from '../pages/menu/menu.module';
import { PopupPageModule } from '../pages/popup/popup.module';
import { AdminHomePageModule } from '../pages/admin-home/admin-home.module';
import { CreateRoutePageModule } from '../pages/create-route/create-route.module';
import { TrackerStartPageModule } from '../pages/tracker-start/tracker-start.module';

import { UrlHelper } from '../helpers/UrlHelper';
import { HeadersHelper } from '../helpers/HeadersHelper';

import { SocketIoModule, SocketIoConfig } from 'ng-socket-io';

import { Geolocation } from '@ionic-native/geolocation';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusProvider } from '../providers/status/status';
import { LoginProvider } from '../providers/login/login';
import { NotificationsProvider } from '../providers/notifications/notifications';
import { ContactProvider } from '../providers/contact/contact';
import { RequestStopProvider } from '../providers/request-stop/request-stop';
import { CreateAdminTrackRouteProvider } from '../providers/create-admin-track-route/create-admin-track-route';
import { TrackerProvider } from '../providers/tracker/tracker';

let remoteUrl = 'http://ec2-18-221-151-141.us-east-2.compute.amazonaws.com:3000/';
let localUrl = 'http://localhost:3000/';
const config: SocketIoConfig = { url: remoteUrl, options: {} };

@NgModule({
  declarations: [
    MyApp,
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    SocketIoModule.forRoot(config),
    HttpClientModule,
    HomePageModule,
    HomePageModule,
    NotificationsPageModule,
    TrackPageModule,
    ContactPageModule,
    WebsitePageModule,
    SoftwarePageModule,
    BackendPageModule,
    ListPageModule,
    MenuPageModule,
    PopupPageModule,
    AdminHomePageModule,
    CreateRoutePageModule,
    TrackerStartPageModule,
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    UrlHelper,
    HeadersHelper,
    StatusProvider,
    LoginProvider,
    NotificationsProvider,
    Geolocation,
    ContactProvider,
    RequestStopProvider,
    CreateAdminTrackRouteProvider,
    TrackerProvider,
    BackgroundMode,
    BackgroundFetch
    // Push
  ]
})
export class AppModule {}
