import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { TrackPage } from '../pages/track/track';
import { NotificationsPage } from '../pages/notifications/notifications';
import { ContactPage } from '../pages/contact/contact';
import { WebsitePage } from '../pages/website/website';
import { SoftwarePage } from '../pages/software/software';
import { BackendPage } from '../pages/backend/backend';

import { StatusProvider } from '../providers/status/status';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = HomePage;

  pages: Array<{title: string, component: any}>;

  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    
    // this.pages = [
    //   { title: 'HOME', component: HomePage },
    //   { title: 'TRACK', component: TrackPage },
    //   { title: 'NOTIFICATIONS', component: NotificationsPage },
    //   { title: 'CONTACT', component: ContactPage },
    //   { title: 'WEBSITE', component: WebsitePage },
    //   { title: 'SOFTWARE', component: SoftwarePage },
    //   { title: 'BACK-END', component: BackendPage }
    // ];

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
}

// ionicBootstrap(MyApp, [StatusProvider]);
