import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TrackerStartPage } from './tracker-start';

@NgModule({
  declarations: [
    TrackerStartPage,
  ],
  imports: [
    IonicPageModule.forChild(TrackerStartPage),
  ],
})
export class TrackerStartPageModule {}
