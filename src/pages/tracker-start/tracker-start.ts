import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams, PopoverController, Platform } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import { Subscription } from 'rxjs/Subscription';
import { filter } from 'rxjs/operators';
import { Socket } from 'ng-socket-io';

import { CreateAdminTrackRouteProvider } from '../../providers/create-admin-track-route/create-admin-track-route';
import { StatusProvider } from '../../providers/status/status';
import { BackgroundFetch, BackgroundFetchConfig } from '@ionic-native/background-fetch';

import { PopupPage } from '../popup/popup';
import { MenuPage } from '../menu/menu';
import { AdminHomePage } from '../admin-home/admin-home';

declare var google;

@IonicPage()
@Component({
  selector: 'page-tracker-start',
  templateUrl: 'tracker-start.html',
})
export class TrackerStartPage {

  @ViewChild('map') mapElement: ElementRef;
  map: any;
  currentMapTrack = null;

  isTracking = false;
  trackedRoute = [];
  previousTracks = [];
  path = [];
  isFirst: boolean = true;
  isLast: boolean = false;
  disLatOne: any;
  disLatTwo: any;
  disLonOne: any;
  disLonTwo: any;
  isCreating: boolean = false;

  status: any = false;
  interval: any;

  positionSubscription: Subscription;
  position: any;
  routeID: number;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private plt: Platform,
    public popoverCtrl: PopoverController,
    public geolocation: Geolocation,
    private statusProvider: StatusProvider,
    private socket: Socket,
    private backgroundFetch: BackgroundFetch,
    private createRouteProvider: CreateAdminTrackRouteProvider) {

    const config: BackgroundFetchConfig = {
      stopOnTerminate: false
    };

    backgroundFetch.configure(config).then(() => {
      alert('Background Fetch initialized');

      this.stopTracking();

      this.backgroundFetch.finish();
     }).catch(e => console.log('Error initializing background fetch', e));
    
    this.socket.on('StopRequest', data => {
      this.presentRequestPopup('event', data);
    });

    this.isCreating = this.navParams.get('create');
    this.position = this.navParams.get('positionSubscription');
    this.routeID = this.navParams.get('id');

    if (this.routeID) {
      this.startTracking();
    }

    // console.log(this.isCreating);
    if (!this.isCreating) {
      console.log('IFF');
      this.stopTracking();
    }

    window.addEventListener('pause', () => {
      alert('INSSSSS');
      // this.updateStatus();
    });
  }

  startTracking() {
    console.log('TRACKING START');
    this.isTracking = true;
    this.trackedRoute = [];

    this.status = true;

    let options = {
        frequency: 1000,
        enableHighAccuracy: true
      };

    this.statusProvider.post({ status: true, date: new Date() }).subscribe(response => {
      if (response != null) {
        this.status = response;
      }
      console.log(this.status);
    }, error => {
      console.log(error);
    });

    localStorage.removeItem('path');
    let paths = this.path.push(JSON.parse(localStorage.getItem('path')));
    console.log(paths);
    localStorage.setItem('path', JSON.stringify(this.path));

    let icon = new google.maps.MarkerImage('assets/imgs/auto.png', null, null, null, new google.maps.Size(50, 30));

    let marker = new google.maps.Marker({
      // position: this.map.getCenter(),
      icon: icon,
      map: this.map
    });

    // this.currentMapTrack.setMapOnAll(null);

    this.positionSubscription = this.geolocation.watchPosition(options)
      .pipe(filter(p => p.coords !== undefined))
      .subscribe(data => {
      setTimeout(() => {
        if (this.isFirst) {
          localStorage.setItem('startLat', data.coords.latitude.toString());
          localStorage.setItem('startLon', data.coords.longitude.toString());

          this.isFirst = false;
        }

        let coords = {
          lat: data.coords.latitude,
          lon: data.coords.longitude,
          date: new Date()
        };

        // alert('TIMEOUT');

        // this.socket.emit('updatePosition', { lat: data.coords.latitude, lng: data.coords.longitude });

        // this.interval = setInterval(() => {
          this.createRouteProvider.updatePosition(coords).subscribe(response => {
            
            // update();
          }, error => {
            console.log(error);
          });
        // }, 1000);

        this.trackedRoute.push({ lat: data.coords.latitude, lng: data.coords.longitude });
        this.path.push({ lat: data.coords.latitude, lng: data.coords.longitude });

        let a = JSON.parse(localStorage.getItem('path'));

        a.push({ lat: data.coords.latitude, lng: data.coords.longitude });

        localStorage.setItem('path', JSON.stringify(a));

        // if (this.isRouteSet) {
        //   // this.isRouteSet = false;
        //   this.redrawPath(this.trackedRoute, marker);
        // } else {
          this.redrawPath(this.trackedRoute, marker);
        // }
      }, 0);
    });
  }

  redrawPath(path, marker) {
    if (this.currentMapTrack) {
      this.currentMapTrack.setMap(null);
    }
 
    if (path.length > 1) {
      this.currentMapTrack = new google.maps.Polyline({
        path: path,
        geodesic: true,
        strokeColor: '#ff00ff',
        strokeOpacity: 1.0,
        strokeWeight: 3
      });

      let latLng = new google.maps.LatLng(path[path.length - 1].lat, path[path.length - 1].lng);
      marker.setPosition(latLng);

      this.currentMapTrack.setMap(this.map);
    }
  }

  getDistanceFromLatLonInKm(destLat,destLon) {
    let startLat = Number(localStorage.getItem('startLat'));
    let startLon = Number(localStorage.getItem('startLon'));

    let R = 6371;
    let dLat = this.deg2rad(destLat - startLat);
    let dLon = this.deg2rad(destLon - startLon);
    let a = 
      Math.sin(dLat/2) * Math.sin(dLat/2) +
      Math.cos(this.deg2rad(startLat)) * Math.cos(this.deg2rad(destLat)) * 
      Math.sin(dLon/2) * Math.sin(dLon/2);

    let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
    let d = R * c;

    return d;
  }

  deg2rad(deg) {
    return deg * (Math.PI/180);
  }

  stopTracking() {
    this.updateStatus();

    alert('TRACKING STOPPED');

    this.geolocation.getCurrentPosition().then(pos => {
      let p = localStorage.getItem('path');

      alert(p);

      let latLng = new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude);

      let newRoute = {
        date: new Date(),
        path: JSON.parse(localStorage.getItem('path')),
        distance: this.getDistanceFromLatLonInKm(pos.coords.latitude, pos.coords.longitude),
        active: false
      };

      alert(JSON.stringify(newRoute));

      this.createRouteProvider.save(newRoute).subscribe(response => {
        this.isTracking = false;
        this.position.unsubscribe();
      }, error => {
        console.log(error);
      });

      this.previousTracks.push(newRoute);
    }).catch(error => {
      console.log('Error getting location', error);
    });
  }

  updateStatus() {
    let newStatus = {
      status: (this.status) ? false : true,
      date: new Date()
    };

    this.statusProvider.update(newStatus).subscribe(response => {
      if (response != null) {
        this.status = response;
      }
    }, error => {
      console.log(error);
    });
  }

  ionViewDidLoad() {
    this.plt.ready().then(() => {
      let mapOptions = {
        zoom: 13,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        mapTypeControl: false,
        streetViewControl: false,
        fullscreenControl: false
      }
      this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
 
      this.geolocation.getCurrentPosition().then(pos => {
        let latLng = new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude);

        this.map.setCenter(latLng);
        this.map.setZoom(16);

        let icon = new google.maps.MarkerImage('assets/imgs/auto.png', null, null, null, new google.maps.Size(50, 30));

        let marker = new google.maps.Marker({
          position: this.map.getCenter(),
          icon: icon,
          map: this.map
        });

        this.startTracking();
      }).catch((error) => {
        console.log('Error getting location', error);
      });
    });
  }

  presentPopover(myEvent) {
    let popover = this.popoverCtrl.create(MenuPage, {
      admin: true,
      create: this.isCreating,
      positionSubscription: this.positionSubscription
    });

    popover.present({
      ev: myEvent
    });
  }

  presentRequestPopup(myEvent, data) {
    let popover = this.popoverCtrl.create(PopupPage, { request: true, data: data });

    popover.onDidDismiss(response => {
      let latLng = new google.maps.LatLng(data.lat, data.lon);
      let icon;

      this.map.setCenter(latLng);
      this.map.setZoom(16);

      if (response == 'true') {
        icon = new google.maps.MarkerImage('assets/imgs/stop-accepted.png', null, null, null, new google.maps.Size(30, 30));
      } else {
        icon = new google.maps.MarkerImage('assets/imgs/stop-rejected.png', null, null, null, new google.maps.Size(30, 30));
      }

      let marker = new google.maps.Marker({
        position: this.map.getCenter(),
        icon: icon,
        map: this.map
      });
    });

    popover.present({
      ev: myEvent
    });
  }

}
