import { Component } from '@angular/core';
import {
  IonicPage,
  NavController,
  NavParams,
  PopoverController } from 'ionic-angular';

import { MenuPage } from '../menu/menu';
import { TrackerStartPage } from '../tracker-start/tracker-start';
import { TrackerProvider } from '../../providers/tracker/tracker';
import { StatusProvider } from '../../providers/status/status';
import { CreateAdminTrackRouteProvider } from '../../providers/create-admin-track-route/create-admin-track-route';

@IonicPage()
@Component({
  selector: 'page-create-route',
  templateUrl: 'create-route.html',
})
export class CreateRoutePage {

  status: any = false;
  tracks: any;
  message: string;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public popoverCtrl: PopoverController,
    private trackerProvider: TrackerProvider,
    private statusProvider: StatusProvider,
    private createRouteProvider: CreateAdminTrackRouteProvider) {

    this.createRouteProvider.getTracks().subscribe(response => {
      console.log(response);
      this.updateDateFormat(response).then(items => {
        this.updateDistance(items).then(item => {
          this.tracks = item;
        })
      });
    }, error => {
      console.log(error);
    });
  }

  updateDateFormat(items) {
    let tracksList = [];
    let index = 0;

    return new Promise((resolve, reject) => {
      items.forEach(item => {
        item['date'] = new Date(item.date).getDate() + '-' + new Date(item.date).getMonth() + '-' + new Date(item.date).getFullYear();

        tracksList.push(item);
        index++;
      });

      if (tracksList.length >= index) {
        resolve(tracksList);
      }
    });
  }

  updateDistance(items) {
    let distance = [];
    let index = 0;

    return new Promise((resolve, reject) => {
      items.forEach(item => {
        item['distance'] = Math.floor(item.distance);

        distance.push(item);
        index++;
      });

      if (distance.length >= index) {
        resolve(distance);
      }
    });
  }

  delete(id) {
    this.createRouteProvider.remove({ id: id }).subscribe(response => {
      // this.message = response;
    });
  }

  ionViewDidLoad() {
    this.statusProvider.get().subscribe(response => {
      if (response != null) {
        this.status = response;
      }
    }, error => {
      console.log(error);
    });
  }

  startTrack(id) {
    this.navCtrl.setRoot(TrackerStartPage, { id: id, create: true });
  }

  presentPopover(myEvent) {
    let popover = this.popoverCtrl.create(MenuPage, { admin: true });

    popover.present({
      ev: myEvent
    });
  }

}