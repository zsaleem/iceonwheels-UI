import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, PopoverController } from 'ionic-angular';
// import { Push, PushObject, PushOptions } from '@ionic-native/push';
import {
  FormBuilder,
  FormGroup,
  Validators } from '@angular/forms';

import { NotificationsProvider } from '../../providers/notifications/notifications';
import { MenuPage } from '../menu/menu';
import { PopupPage } from '../popup/popup';
import { StatusProvider } from '../../providers/status/status';

@IonicPage()
@Component({
  selector: 'page-notifications',
  templateUrl: 'notifications.html',
})
export class NotificationsPage {

  notificationForm: FormGroup;
	route: boolean;
  proximity: boolean;
  arrival: boolean;
  fields: any;
  isNotifying: boolean = false;
  status: any = false;
  error;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public popoverCtrl: PopoverController,
    private formBuilder: FormBuilder,
    private statusProvider: StatusProvider,
    // private push: Push,
    private notificationsProvider: NotificationsProvider) {

    this.isNotifying = true;

    this.notificationForm = this.formBuilder.group({
      route: [false],
      proximity: [false],
      arrival: [false]
    });

    this.notificationsProvider.get(localStorage.getItem('userid')).subscribe(response => {
      console.log(response);
      if (response != undefined) {
        this.fields = response;
        this.isNotifying = false;
        if (this.fields.length != 0) {
          this.notificationForm.patchValue({
            route: this.fields[0].route,
            proximity: this.fields[0].proximity,
            arrival: this.fields[0].arrival
          });
        }
      }
    }, error => {
      this.isNotifying = false;
      if (error.status == 401) {
          alert('401');
      }
    });
  }

  ionViewDidLoad() {
    this.statusProvider.get().subscribe(response => {
      if (response != null) {
        this.status = response;
      }
    }, error => {
      console.log(error);
    });
  }

  presentPopup(myEvent) {
    let popup = this.popoverCtrl.create(PopupPage, { form: false, title: 'Notifications updated.', btnLabelOk: 'OK' });

    popup.present({
      ev: myEvent
    });
  }

  presentPopover(myEvent) {
    let popover = this.popoverCtrl.create(MenuPage, { admin: false });

    popover.present({
      ev: myEvent
    });
  }

  update() {
    this.isNotifying = true;
    let options = {};

    options['route'] = this.notificationForm.controls['route'].value;
    options['proximity'] = this.notificationForm.controls['proximity'].value;
    options['arrival'] = this.notificationForm.controls['arrival'].value;
    options['token'] = localStorage.getItem('notificationToken');

    this.notificationsProvider.update(options).subscribe(response => {
      this.isNotifying = false;
      this.presentPopup('event');
    }, error => {
      this.isNotifying = false;
      let self = this;
      self.error = error.message;

      setTimeout(function() {
        self.error = false;
      }, 5000);
    });
  }

}
