import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams, PopoverController } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import { Socket } from 'ng-socket-io';

import { PopupPage } from '../popup/popup';
import { MenuPage } from '../menu/menu';

import { StatusProvider } from '../../providers/status/status';
import { CreateAdminTrackRouteProvider } from '../../providers/create-admin-track-route/create-admin-track-route';

declare var google;

@IonicPage()
@Component({
  selector: 'page-track',
  templateUrl: 'track.html',
})
export class TrackPage {

  @ViewChild('map') mapElement: ElementRef;
  map: any;
  status: any = false;
  response: any;
  currentMapTrack = null;
  mapTrack = null;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private statusProvider: StatusProvider,
    public popoverCtrl: PopoverController,
    private socket: Socket,
    private createRouteProvider: CreateAdminTrackRouteProvider,
    public geolocation: Geolocation) {
    // this.presentPopup('event');
    this.socket.on('updatePosition', data => {
      this.trackCart(data);
    });

    this.createRouteProvider.getPath().subscribe(response => {
      this.drawPath(response);
    });
  }

  ionViewDidLoad() {
    this.statusProvider.get().subscribe(response => {
      if (response != null) {
        this.status = response;
      }
    }, error => {
      console.log(error);
    });
    
    this.loadMap();
  }

  trackCart(data) {
    console.log(data);

    let latLng = new google.maps.LatLng(data.lat, data.lon);

    let mapOptions = {
      center: latLng,
      zoom: 15,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);

    let icon = new google.maps.MarkerImage('assets/imgs/auto.png', null, null, null, new google.maps.Size(50, 30));

    let marker = new google.maps.Marker({
      // position: this.map.getCenter(),
      icon: icon,
      map: this.map
    });

    marker.setPosition(latLng);
  }

  loadMap() {
    this.geolocation.getCurrentPosition().then(position => {
      let latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);

      let mapOptions = {
        center: latLng,
        zoom: 16,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      };

      this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);

    }, err => {
      console.log(err);
    });
  }

  drawPath(path) {
    console.log(path);
    let index = path.path.indexOf(null);
    path.splice(index, 1);
    // if (this.map) {
    //   this.currentMapTrack.setMap(null);
    // }

    console.log(path);

    var flightPlanCoordinates = [
          {lat: 37.772, lng: -122.214},
          {lat: 21.291, lng: -157.821},
          {lat: -18.142, lng: 178.431},
          {lat: -27.467, lng: 153.027}
        ];

 
    if (path.length > 1) {
      this.mapTrack = new google.maps.Polyline({
        path: path,
        geodesic: true,
        strokeColor: '#ff00ff',
        strokeOpacity: 1.0,
        strokeWeight: 3
      });

      this.mapTrack.setMap(this.map);

      // let latLng = new google.maps.LatLng(path[path.length - 1].lat, path[path.length - 1].lng);
      // marker.setPosition(latLng);

      // this.currentMapTrack.setMap(this.map);
    }
  }

  demandStop() {
    this.presentPopup('event');
  }

  presentPopover(myEvent) {
    let popover = this.popoverCtrl.create(MenuPage, { admin: false });

    popover.present({
      ev: myEvent
    });
  }

  presentPopup(myEvent) {
    let popup = this.popoverCtrl.create(PopupPage, { form: true, nameLabel: 'Name', mobileLabel: 'Mobile' });

    popup.onDidDismiss(response => {
      setTimeout(() => {
        this.response = false;
      }, 5000);
      this.response = response;
    });

    popup.present({
      ev: myEvent
    });
  }

}