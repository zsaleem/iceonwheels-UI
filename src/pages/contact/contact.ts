import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, PopoverController } from 'ionic-angular';
import {
  FormBuilder,
  FormGroup,
  Validators } from '@angular/forms';

import { MenuPage } from '../menu/menu';

import { ContactProvider } from '../../providers/contact/contact';
import { StatusProvider } from '../../providers/status/status';

@IonicPage()
@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html',
})
export class ContactPage {

  contactForm: FormGroup;
  name: string;
  email: string;
  mobile: number;
  message: string;
  error: boolean = false;
  msg: string = '';
  isSending: boolean = false;
  status: any = false;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public popoverCtrl: PopoverController,
    private formBuilder: FormBuilder,
    private contactProvider: ContactProvider,
    private statusProvider: StatusProvider) {

    this.contactForm = this.formBuilder.group({
      name: ['', Validators.required],
      email: ['', Validators.required],
      mobile: ['', Validators.required],
      message: ['', Validators.required]
    });
  }

  ionViewDidLoad() {
    this.statusProvider.get().subscribe(response => {
      console.log(response);
      if (response != null) {
        this.status = response;
      }
    }, error => {
      console.log(error);
    });
  }

  send() {
    this.isSending = true;

    let message = {};

    message['name'] = this.contactForm.controls['name'].value;
    message['email'] = this.contactForm.controls['email'].value;
    message['mobile'] = this.contactForm.controls['mobile'].value;
    message['message'] = this.contactForm.controls['message'].value;

    this.contactProvider.send(message).subscribe(response => {
      this.isSending = false;
      this.error = false;

      console.log(response);
    }, error => {
      let self = this;
      this.error = true;
      this.isSending = false;
      this.msg = error.message;

      setTimeout(() => {
        self.error = false;
      }, 5000);
    });
  }

  presentPopover(myEvent) {
    let popover = this.popoverCtrl.create(MenuPage, { admin: false });

    popover.present({
      ev: myEvent
    });
  }

}
