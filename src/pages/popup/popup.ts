import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { RequestStopProvider } from '../../providers/request-stop/request-stop';
import { Geolocation } from '@ionic-native/geolocation';

import {
  FormBuilder,
  FormGroup,
  Validators } from '@angular/forms';

@IonicPage()
@Component({
  selector: 'page-popup',
  templateUrl: 'popup.html',
})
export class PopupPage {

  requestStopForm: FormGroup;
	message: string = '';
	btnLabelOne: string = '';
	btnLabelTwo: string = '';
  nameLabel: string = '';
  mobileLabel: string = '';
  btnLabelOk: string = '';
  form: boolean = false;
  error: boolean = false;
  errorMessage: string = '';
  isRequesting: boolean = false;
  request: boolean = false;
  items: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private formBuilder: FormBuilder,
    private requestStopProvider: RequestStopProvider,
    public geolocation: Geolocation,
    public viewCtrl: ViewController) {

  	this.message = this.navParams.get('title');
  	this.btnLabelOne = this.navParams.get('btnYes');
  	this.btnLabelTwo = this.navParams.get('btnNo');
    this.btnLabelOk = this.navParams.get('btnLabelOk');
    this.request = this.navParams.get('request');
    this.items = this.navParams.get('data');

    if (this.navParams.get('form')) {
      this.form = true;

      this.requestStopForm = this.formBuilder.group({
        name: ['', Validators.required],
        mobile: ['', Validators.required],
        street: ['', Validators.required]
      });
    }
  }

  ionViewDidLoad() {
  }

  requestStop() {
    this.isRequesting = true;
    let stopItems = {};

    stopItems['name'] = this.requestStopForm.controls['name'].value;
    stopItems['mobile'] = this.requestStopForm.controls['mobile'].value;
    stopItems['street'] = this.requestStopForm.controls['street'].value;

    this.geolocation.getCurrentPosition().then(position => {
      stopItems['lat'] = position.coords.latitude;
      stopItems['lon'] = position.coords.longitude;
      stopItems['date'] = new Date();

      this.requestStopProvider.post(stopItems).subscribe(response => {
        this.close(response);
        this.error = false;
        this.isRequesting = false;
      }, error => {
        alert(JSON.stringify(error));
        this.error = true;
        this.isRequesting = false;
        this.errorMessage = error.message;
      });
    }, err => {
      console.log(err);
    });
  }

  acceptStop(response) {
    this.close(response);
  }

  rejectStop(response) {
    this.close(response);
  }

  close(response) {
    this.viewCtrl.dismiss(response);
  }

}
