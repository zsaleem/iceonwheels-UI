import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, PopoverController } from 'ionic-angular';

import { MenuPage } from '../menu/menu';
import { StatusProvider } from '../../providers/status/status';

@IonicPage()
@Component({
  selector: 'page-software',
  templateUrl: 'software.html',
})
export class SoftwarePage {

  status: any = false;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private statusProvider: StatusProvider,
    public popoverCtrl: PopoverController) {
  }

  ionViewDidLoad() {
    this.statusProvider.get().subscribe(response => {
      if (response != null) {
        this.status = response;
      }
    }, error => {
      console.log(error);
    });
  }

  presentPopover(myEvent) {
    let popover = this.popoverCtrl.create(MenuPage, { admin: false });

    popover.present({
      ev: myEvent
    });
  }

}
