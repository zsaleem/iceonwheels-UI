import { Component } from '@angular/core';
import {
  IonicPage,
  NavController,
  NavParams,
  LoadingController,
  Loading,
  AlertController,
  PopoverController } from 'ionic-angular';

import {
  FormBuilder,
  FormGroup,
  Validators } from '@angular/forms';

import { StatusProvider } from '../../providers/status/status';

import { MenuPage } from '../menu/menu';
import { AdminHomePage } from '../admin-home/admin-home';

import { LoginProvider } from '../../providers/login/login';

@IonicPage()
@Component({
  selector: 'page-backend',
  templateUrl: 'backend.html',
})
export class BackendPage {

  loginForm: FormGroup;
	loading: Loading;
  isLogging: boolean = false;
  error: boolean;
  errorMessage: any;
  login: string;
  password: string;
  status: any = false;

  constructor(
  		public navCtrl: NavController,
  		public navParams: NavParams,
  		private loginProvider: LoginProvider,
  		private loadingCtrl: LoadingController,
      private alertCtrl: AlertController,
      private formBuilder: FormBuilder,
      public popoverCtrl: PopoverController,
      private statusProvider: StatusProvider) {

    this.loginForm = this.formBuilder.group({
      login: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  loginUser() {
    this.isLogging = true;

    let loginItems = {};

    loginItems['login'] = this.loginForm.controls['login'].value;
    loginItems['password'] = this.loginForm.controls['password'].value;

    // if (loginItems.login && loginItems.password) {
      // this.navCtrl.setRoot(AdminHomePage);
    // }

    this.loginProvider.login(loginItems).subscribe(response => {
      localStorage.removeItem('token');
      localStorage.removeItem('login');
      localStorage.removeItem('userid');

      // console.log(response);

      if (response.success) {
        localStorage.setItem('token', response.token);
        localStorage.setItem('login', response.login);
        localStorage.setItem('userid', response.id);

        this.navCtrl.setRoot(AdminHomePage);

        return;
      }

      this.error = true;
      this.errorMessage = response;
      this.isLogging = false;
    }, error => {
      this.error = true;
      this.errorMessage = error;
      this.isLogging = false;
    });
  }

  showLoading() {
	  this.loading = this.loadingCtrl.create({
      content: 'Please wait...',
      dismissOnPageChange: true
	  });

	  this.loading.present();
	}

	hideLoading() {
    this.loading.dismiss();
  }

  ionViewDidLoad() {
    this.statusProvider.get().subscribe(response => {
      if (response != null) {
        this.status = response;
      }
    }, error => {
      console.log(error);
    });
  }

  presentPopover(myEvent) {
    let popover = this.popoverCtrl.create(MenuPage);

    popover.present({
      ev: myEvent
    });
  }

}
