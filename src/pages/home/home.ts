import { Component } from '@angular/core';
import { NavController, MenuController, PopoverController } from 'ionic-angular';
import { NotificationsProvider } from '../../providers/notifications/notifications';
import { Socket } from 'ng-socket-io';

import { MenuPage } from '../menu/menu';
import { StatusProvider } from '../../providers/status/status';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  status: any = false;

  constructor(
    public navCtrl: NavController,
    private menu: MenuController,
    public popoverCtrl: PopoverController,
    private statusProvider: StatusProvider,
    private socket: Socket,
    private notificationsProvider: NotificationsProvider) {
  	
    this.menu.enable(false);

    // this.socket.on('StatusUpdated', this.updateStatus);

    // if (localStorage.getItem('notificationToken') == 'undefined' || localStorage.getItem('notificationToken') == null) {
    //   this.notificationsProvider.getToken().subscribe(response => {
    //     if (response.hasOwnProperty('notificationToken')) {
    //       console.log(response);
    //       localStorage.setItem('notificationToken', response.notificationToken);
    //     }
    //   }, error => {
    //     console.log(error);
    //   });
    // }
  }

  ionViewDidLoad() {
    this.statusProvider.get().subscribe(response => {
      if (response != null) {
        this.status = response;
      }
    }, error => {
      console.log(error);
    });
  }

  updateStatus() {
    
  }

  presentPopover(myEvent) {
    let popover = this.popoverCtrl.create(MenuPage, { admin: false });

    popover.present({
      ev: myEvent
    });
  }

}
