import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

import { TrackPage } from '../track/track';
import { NotificationsPage } from '../notifications/notifications';
import { ContactPage } from '../contact/contact';
import { WebsitePage } from '../website/website';
import { SoftwarePage } from '../software/software';
import { BackendPage } from '../backend/backend';
import { CreateRoutePage } from '../create-route/create-route';
import { TrackerStartPage } from '../tracker-start/tracker-start';

@IonicPage()
@Component({
  selector: 'page-menu',
  templateUrl: 'menu.html',
})
export class MenuPage {

  isAdmin: boolean = false;
  isCreating: boolean = false;
  positionSubscription: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController) {
    this.isAdmin = (this.navParams.get('admin')) ? true : false;
    this.isCreating = this.navParams.get('create');
    this.positionSubscription = this.navParams.get('positionSubscription');
  }

  ionViewDidLoad() {
  }

  close(page) {
  	this.viewCtrl.dismiss();

  	if (page == 'track') {
  		this.navCtrl.setRoot(TrackPage);
  	} else if (page == 'notifications') {
  		this.navCtrl.setRoot(NotificationsPage);
  	} else if (page == 'contact') {
  		this.navCtrl.setRoot(ContactPage);
  	} else if (page == 'website') {
  		this.navCtrl.setRoot(WebsitePage);
  	} else if (page == 'software') {
  		this.navCtrl.setRoot(SoftwarePage);
  	} else if (page == 'create') {
      if (this.isCreating) {
        this.isCreating = false;
      } else {
        this.isCreating = true;
      }
      
      this.navCtrl.setRoot(TrackerStartPage, { create: this.isCreating, positionSubscription: this.positionSubscription });
    } else if (page == 'tracker') {
      this.navCtrl.setRoot(CreateRoutePage);
    } else {
  		this.navCtrl.setRoot(BackendPage);
  	}
  }

}
