import { HttpHeaders } from '@angular/common/http';

export class HeadersHelper {
  public getHttpHeaders() {
    return new HttpHeaders({
      'Content-Type': 'application/json',
      'withCredentials': 'true',
      'Authorization': localStorage.getItem('token')
    });
  }
}
