export class UrlHelper {
  remote: string = 'http://ec2-18-221-151-141.us-east-2.compute.amazonaws.com:3000/api/1.0/';
  local: string = 'http://localhost:3000/api/1.0/';
  ip: string = 'http://18.207.221.252/api/1.0/'

  setUrl(endPoint) {
    return this.remote + endPoint;
  }
}
