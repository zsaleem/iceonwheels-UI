import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { UrlHelper } from '../../helpers/UrlHelper';
import {
	HttpClient,
  HttpParams } from '@angular/common/http';

@Injectable()
export class LoginProvider {
	url: string = 'http://localhost:3000/api/1.0/authentication';

  constructor(public http: HttpClient, private urlHelper: UrlHelper) {
  }

  login(credentials):Observable<any> {
    return this.http.post(this.urlHelper.setUrl('authenticate'), { login: credentials.login, password: credentials.password });
  }
}
