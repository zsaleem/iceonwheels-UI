import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { UrlHelper } from '../../helpers/UrlHelper';

import {
	HttpClient,
  HttpParams,
  HttpHeaders } from '@angular/common/http';

@Injectable()
export class NotificationsProvider {

  constructor(public http: HttpClient, private urlHelper: UrlHelper) {}

  update(settings) {
  	console.log(settings);
  	return this.http.post(this.urlHelper.setUrl('notifications'), settings);
  }

  get(userid) {
    let queryString = new HttpParams().set('userid', userid);
    let headers = new HttpHeaders({
        'Content-Type': 'application/json'
      });

    let options = {
        headers: headers,
        params: queryString
      };
  	// console.log('012838912738912');
  	return this.http.get(this.urlHelper.setUrl('notifications'), options);
  }

  getToken() {
    return this.http.post(this.urlHelper.setUrl('notifications/token'), {});
  }

}
