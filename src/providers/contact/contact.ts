import { Injectable } from '@angular/core';
import { UrlHelper } from '../../helpers/UrlHelper';

import {
	HttpClient,
  HttpParams } from '@angular/common/http';

@Injectable()
export class ContactProvider {

  constructor(public http: HttpClient, private urlHelper: UrlHelper) {}

  send(message) {
  	console.log(message);
  	return this.http.post(this.urlHelper.setUrl('contact'), message);
  }

}
