import { Injectable } from '@angular/core';
import { UrlHelper } from '../../helpers/UrlHelper';

import {
	HttpClient,
  HttpParams,
  HttpHeaders } from '@angular/common/http';

@Injectable()
export class RequestStopProvider {

  constructor(public http: HttpClient, private urlHelper: UrlHelper) {
    console.log('Hello DemandStopProvider Provider');
  }

  post(stopItem) {
  	return this.http.post(this.urlHelper.setUrl('request'), stopItem);
  }

}
