import { Injectable } from '@angular/core';
import { UrlHelper } from '../../helpers/UrlHelper';
import { HeadersHelper } from '../../helpers/HeadersHelper';

import {
	HttpClient,
  HttpParams } from '@angular/common/http';

@Injectable()
export class TrackerProvider {

  constructor(
  	public http: HttpClient,
  	private urlHelper: UrlHelper,
  	private headersHelper: HeadersHelper) {}

  get() {
  	// let queryString = new HttpParams().set('eventid', 'eventIDParam'),
      let options = {
        headers: this.headersHelper.getHttpHeaders()
      };

    return this.http.get(this.urlHelper.setUrl('tracker-list'), options);
  }

}
