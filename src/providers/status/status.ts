import { Injectable } from '@angular/core';
import { UrlHelper } from '../../helpers/UrlHelper';
import { HeadersHelper } from '../../helpers/HeadersHelper';

import {
	HttpClient,
  HttpParams,
  HttpHeaders } from '@angular/common/http';

@Injectable()
export class StatusProvider {

  constructor(
  	public http: HttpClient,
  	private urlHelper: UrlHelper,
  	private headersHelper: HeadersHelper) {}

  update(status) {
  	let options = {
      headers: this.headersHelper.getHttpHeaders()
    };

    return this.http.post(this.urlHelper.setUrl('status'), status, options);
  }

  post(status) {
    let options = {
      headers: this.headersHelper.getHttpHeaders()
    };

    return this.http.post(this.urlHelper.setUrl('status'), status, options);
  }

  get() {
    let headers = new HttpHeaders({
        'Content-Type': 'application/json'
      });

    let options = {
        headers: headers
      };

  	return this.http.get(this.urlHelper.setUrl('status'), options);
  }

}
