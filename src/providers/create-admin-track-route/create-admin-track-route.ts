import { Injectable } from '@angular/core';
import { UrlHelper } from '../../helpers/UrlHelper';
import { HeadersHelper } from '../../helpers/HeadersHelper';

import {
	HttpClient,
  HttpParams } from '@angular/common/http';

@Injectable()
export class CreateAdminTrackRouteProvider {

  constructor(
  	public http: HttpClient,
  	private urlHelper: UrlHelper,
  	private headersHelper: HeadersHelper) {}

  save(path) {
  	let options = {
        headers: this.headersHelper.getHttpHeaders()
    	};

    return this.http.post(this.urlHelper.setUrl('routes'), path, options);
  }

  updatePosition(position) {
    console.log(position);
    let options = {
        headers: this.headersHelper.getHttpHeaders()
      };

    return this.http.post(this.urlHelper.setUrl('track'), position, options);
  }

  getTrackedPosition() {
    return this.http.get(this.urlHelper.setUrl('track')); 
  }

  getPath() {
    return this.http.get(this.urlHelper.setUrl('routes/path')); 
  }

  getTracks() {
    let options = {
        headers: this.headersHelper.getHttpHeaders()
      };

    return this.http.get(this.urlHelper.setUrl('routes/tracks'), options);
  }

  remove(item) {
    let options = {
      headers: this.headersHelper.getHttpHeaders()
    };

    return this.http.post(this.urlHelper.setUrl('routes/delete'), item, options);
  }
}
